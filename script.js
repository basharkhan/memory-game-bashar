const gameContainer = document.getElementById("game");

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple",
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want to research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

// TODO: Implement this function!

let previousBoxElement = null;
let stopper = false;
let count = 0;

function handleCardClick(event){


  if(stopper == true || event.target.classList.contains("done")){
    return
  }
  
  event.target.classList.add("done");
  
  let currentBoxElement = event.target;
  
  let colorValue = currentBoxElement.classList[0];
  currentBoxElement.style.backgroundColor = colorValue;
  
  if(previousBoxElement == null){
    previousBoxElement = currentBoxElement;
  }

  else{

    if(previousBoxElement.classList[0] == colorValue){
      previousBoxElement = null;
      count++;
      if(count == 5) alert("game over");
    }

    else{
      stopper = true;
      setTimeout(() => {
        

        previousBoxElement.classList.remove("done");
        previousBoxElement.style.backgroundColor = "white";
        
        currentBoxElement.classList.remove("done");
        currentBoxElement.style.backgroundColor = "white";
        previousBoxElement = null;
        stopper = false;
      }, 1000);
      
    }
    
  }
  
  
}

// when the DOM loads
createDivsForColors(shuffledColors);
